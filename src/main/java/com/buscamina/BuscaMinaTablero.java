package com.buscamina;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Antonio
 */
public class BuscaMinaTablero {
    
    //Variables y objetos.
    BuscaMinaCelda[][] tablero;
    Logger registro = Logger.getLogger("");
    byte CONTADOR_MINA = 9;
    int  horizontal = 0;
    int vertical = 0;
    int minas = 0;
    boolean partida = false;
    
    /**
     * Se crea un tablero con el numero de filas y columnas deseadas y se le 
     * añaden el número de minas que contendra.
     * @param horizontal Es el numero de celdas en horizontal del tablero.
     * @param vertical Es el numero de celdas en vertical del tablero.
     * @param minas Es el numero de minas del tablero.
     */
    BuscaMinaTablero(int horizontal, int vertical, int minas){
        
        //Variables para utilizar en otros métodos.
        this.horizontal = horizontal;
        this.vertical = vertical;
        this.minas = minas;
        
        //Creamos el tablero vertical lo reyenamos con celdas.
        //Las variables horizontal y vertical las creo porque las utilizare en otros métodos.
        this.tablero = new BuscaMinaCelda[horizontal][vertical];
        //El número de minas nunca podrá ser superior al número de celdas.
        if((horizontal * vertical) >= minas){
        }
        else{
            registro.log(Level.WARNING, "Las minas introducidas son superiores "
                    + "a las celdas del tablero." + "\n" + "Las minas se han "
                    + "igualado a las celdas.", minas);
            minas = horizontal * vertical;

        }
        for(int i=0; i<tablero.length; i++){
            for(int j=0; j<tablero[0].length; j++){
                BuscaMinaCelda celda = new BuscaMinaCelda();
                tablero[i][j] = celda;
            }
        }
        
        //Introducimos las minas en el tablero.
        Random aleatorio = new Random();
        int contadorMinas = 0;
        //Generamos la posición aleatoria horizontal y vertical comprobamos sino hay una mina.
        do{
            int posicionX = aleatorio.nextInt(horizontal);
            int posicionY = aleatorio.nextInt(vertical);
            String posicion = "(" + String.valueOf(posicionY) + "/" 
                    + String.valueOf(posicionX) + ")";
            
            if(!tablero[posicionX][posicionY].getMina()){
                tablero[posicionX][posicionY].setMina(true);
                contadorMinas++; 
                registro.log(Level.FINEST, "Se ha generado una mina, en la "
                        + "posicion " + posicion);
            }
            else{
                registro.log(Level.FINEST, "Se ha intentado poner un a mina, "
                        + "pero ya existia una en la posicion " + posicion);
            }
        }while(contadorMinas<minas);
 
        //Contamos las minas que colindan con cada celda.
        //recorremos toda la matriz.
        byte contador = 0;
        for(int i=0; i<tablero.length; i++){
            for(int j=0; j<tablero[0].length; j++){
                //Miramos si la celda contiene una mina
                contador = 0;    
                //no hay mina
                if(!tablero[i][j].getMina()){
                    //contamos las minas colindantes de cada celda
                    for(int q=i-1; q<=i+1; q++){
                        for(int w=j-1; w<=j+1; w++){  
                            //comprobar si existen todas las celdas
                            if(q >= 0 && q < horizontal && w >= 0 && w < vertical){
                                //si hay mina
                                if(tablero[q][w].getMina()){
                                    tablero[i][j].setContador(++contador);
                                }
                            }
                        }
                    }
                }
                //si hay mina
                else{
                    tablero[i][j].setContador(CONTADOR_MINA);
                }
                String posicionMatriz = "(" + String.valueOf(i) + "/" 
                        + String.valueOf(j) + ")";    
                registro.log(Level.FINEST, "En la posición " + posicionMatriz
                + " hay " + tablero[i][j].getContador() + " minas.");
            }
        }
        //Mostrar el tablero del juego conteniendo celdas con minas (*) y celdas sin minas(.).
        registro.log(Level.WARNING, this.toString()); 
    }
    
    
    /**
     * Se muestra el tablero con un * donde hay una mina y un . donde no hay mina;
     * @return Devuelve el mostrarMinas que contendra la matriz
     */
    public String toString(){
        //Variables.
        String numeroMinasAlrrededor = "\nMines Counter";
        String mostrarMinas ="Mines";
        String estadoMinas = "\nEstado Minas";
        byte estado = 0;
        String finDePartida = "\nLa partida continua.";

        for(int i=0; i<horizontal; i++){
            numeroMinasAlrrededor += "\n";
            mostrarMinas += "\n";
            estadoMinas += "\n";
            for(int j=0; j<vertical; j++){
                numeroMinasAlrrededor += tablero[j][i].getContador();
                estado = tablero[j][i].getEstado();
                switch(estado){
                    case BuscaMinaCelda.SIN_DESCUBRIR:{
                        estadoMinas += "@";
                        break;
                    }
                    case BuscaMinaCelda.DESCUBRIERTA:{
                        estadoMinas += "#";
                        break;
                    }
                    case BuscaMinaCelda.BANDERITA:{
                        estadoMinas += "?";
                        break;
                    }
                }
                if(tablero[j][i].getMina()){
                    mostrarMinas += "*";
                }
                else{
                    mostrarMinas += ".";
                }
            }
        }
        String contadorBanderas = "\nHay " + Byte.toString(this.contadorBanderas()) + " banderas.";
        if(partida){
            finDePartida = "\nLa partida ha terminado --HAS GANADO--.";
        }
        registro.log(Level.WARNING, finDePartida);
        return mostrarMinas + numeroMinasAlrrededor + estadoMinas + contadorBanderas;
    }
    
    /**
     * Contar el número de banderas.
     * @return Devuelve el número de banderas colocadas.
     */
    byte contadorBanderas(){

        //Variables.
        byte contador = 0;

        for(BuscaMinaCelda fila[]:tablero){
            for(BuscaMinaCelda celda:fila){
                if(celda.getEstado() == BuscaMinaCelda.BANDERITA){
                    contador++;
                }
            }
        }
        return contador;
    }
    
    
    /**
     * Pondrá o quitara una banderita en la posición escogida.
     * @param coordenadaX Posición escogida en el eje X.
     * @param coordenadaY Posición escogida en el eje Y.
     * @return Devuelve si ha colocado una banderita o no.
     */
    public boolean colocarBanderita(int coordenadaX, int coordenadaY){

        //Variables.
        boolean banderita = false;
        String posicion = "(" + String.valueOf(coordenadaY) + "/" + String.valueOf(coordenadaX) + ")";

        registro.log(Level.FINEST, "Es la posición " + posicion);
        //La celda está descubierta y por eso no se puede colocar una banderita.
        if(tablero[coordenadaX][coordenadaY].getEstado() == BuscaMinaCelda.DESCUBRIERTA){
            registro.log(Level.FINEST, "La celda " + posicion + " ya está descubierta.");
            //Ya devuelve que no ha puesto la banderita porque la celda está descubierta. 
        }
        else{
            //Se ha quitado la banderita porque ya habia una colocada he dicha posición.
            if(tablero[coordenadaX][coordenadaY].getEstado() == BuscaMinaCelda.BANDERITA){
                registro.log(Level.FINEST, "La celda " + posicion + " se ha quitado la banderita y se ha ocultado la celda.");
                tablero[coordenadaX][coordenadaY].setEstado(BuscaMinaCelda.SIN_DESCUBRIR);
                //Ya devuelve que no ha puesto la banderita porque la ha quitado.
            }
            //Se coloca la banderita.
            else{
                tablero[coordenadaX][coordenadaY].setEstado(BuscaMinaCelda.BANDERITA);
                registro.log(Level.FINEST, "La celda " + posicion + " se coloco la banderita.");
                banderita = true;
            }
        }
        registro.log(Level.WARNING, this.toString());
        return banderita;
    };
    
    
    /**
     * Descubrir la celda vertical mostrar lo que contiene.
     * @param coordenadaX Es la posición de la fila que queremos descubrir.
     * @param coordenadaY Es la posición de la columna que queremos descubrir.
     * @return Devuelve si hay una mina en dicha posición o no.
     */
    boolean descubrirCelda(int coordenadaX, int coordenadaY){

        //Variables.
        String posicion = "(" + coordenadaX + "/" + coordenadaY + ")";
        boolean mina = false;

        registro.log(Level.FINEST, "La posición seleccionada es " + posicion);
        //descubrimos la celda elegida
        if(tablero[coordenadaX][coordenadaY].getEstado() == BuscaMinaCelda.SIN_DESCUBRIR){
            tablero[coordenadaX][coordenadaY].setEstado(BuscaMinaCelda.DESCUBRIERTA);
            //En esta celda hay una mina
            if(tablero[coordenadaX][coordenadaY].getMina()){ 
                //Le doy el valor asi. porque ya he preguntado si esto es una mina.
                mina = true;
                registro.log(Level.FINEST, "Hay una mina. " + posicion);
            }
            else{
                //si hay una mina alrrededor.
                //He quitado el valor de la mina porque siempre sera falso en estos casos.
                if(tablero[coordenadaX][coordenadaY].getContador() > 0){
                    registro.log(Level.FINEST, "Hay una/s mina/s alrrededor de la posicion " + posicion);
                }
                else{
                    for(int i=coordenadaX-1; i<=coordenadaX+1; i++){
                        for(int j=coordenadaY-1; j<=coordenadaY+1; j++){
                            if(i >= 0 && i < horizontal && j >= 0 && j < vertical){
                                if(tablero[i][j].getEstado() == BuscaMinaCelda.SIN_DESCUBRIR){
                                    if(tablero[i][j].getContador() == 0){
                                        registro.log(Level.FINEST, "En la posición (" + i + "/" + j + ") no hay una mina.");
                                    }
                                    this.descubrirCelda(i, j);
                                }
                            }
                        }
                    }
                }
            }         
        }
        registro.log(Level.WARNING, this.toString());
        return mina;
    };
    
    
    /**
     * Terminara el juego cuando el número de celdas descubiertas más el número de 
     * minas iguale al número de celdas del tablero.
     * @return Si la partida finaliza o continua.
     */
    boolean finPartida(){
        
        //Variables.
        int contadorDescubiertas = 0;
        
        for(int i=0; i<horizontal; i++){
            for(int j=0; j<vertical; j++){
                if(tablero[i][j].getEstado() == BuscaMinaCelda.DESCUBRIERTA){
                    contadorDescubiertas++;
                }
            }
        }
        if(horizontal * vertical == minas + contadorDescubiertas){
            partida = true;
        }
        return partida;
    }
    
    
    /**
     * Es el encargado de mostrar todas las celdas descubiertas.
     */
    public void abandonar(){
        
        for(int i=0; i<horizontal; i++){
            for(int j=0; j<vertical; j++){
                if(tablero[i][j].getEstado() != BuscaMinaCelda.DESCUBRIERTA){
                    tablero[i][j].setEstado(BuscaMinaCelda.DESCUBRIERTA);
                }
            }
        }
    }
    
}
