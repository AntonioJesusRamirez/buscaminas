package com.buscamina;

/**
 *
 * @author Antonio
 */
public class BuscaMinaCelda {
    
    boolean mina;
    byte contador;
    //variables de estado para cada celda.
    byte estado;
    //la celda todavia no ha sido descubierta.
    public static final byte SIN_DESCUBRIR = 0;
    //la celda ha sido descubierta por el usuario.
    public static final byte DESCUBRIERTA = 1;
    //la celda ha sido marcado como contenedora de una posible mina.
    public static final byte BANDERITA = 2;
    
    //metodo constructor
    BuscaMinaCelda(){
        
        mina = false;
        contador = 0;
        estado = SIN_DESCUBRIR;
        
    }
    /**
     * Cambiar el estado de la mina.
     * @param mina Es un boolean para indicar si en dicha celda hay una mina o no.
     */
    void setMina(boolean mina){
        this.mina = mina;
    }
    
    /**
     * Esto cambiara el número de minas que tiene alrrededor la celda.
     * @param contador Son las minas que hay alrrededor e la celda.
     */
    void setContador (byte contador){
        this.contador = contador;
    }
    
    /**
     * Se cambaira el estado de la celda.
     * @param estado Los valores que se puede utilizar son SIN_DESCUBRIR = 0, 
     * DESCUBIERTA = 1 o BANDERITA = 2.
     */
    void setEstado(byte estado){
        this.estado = estado;
    }
    
    /**
     * Devuelve si hay una mina o no.
     * @return Devuelve si hay una mina.
     */
    boolean getMina(){
        return mina;
    }
    
    /**
     * Contará las minas que hay en las celdas colindantes.
     * @return Devuelve el número de minas que hay alrrededor.
     */
    byte getContador(){            
        return contador;
    }
    
    /**
     * Devuelve el estado de la celda.
     * @return Puede devolver los siguientes valores son SIN_DESCUBRIR = 0, 
     * DESCUBIERTA = 1 o BANDERITA = 2.
     */
    byte getEstado(){
        return estado;
    } 
}


