package com.buscamina;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author Antonio
 */
public class BuscaMinaDibujo extends VBox{
    
    //Objetos y variables.
    public static GridPane root = new GridPane();
    HBox hboxInferior = new HBox(); 
    public static BuscaMinaTablero buscaMinaTablero;
    int horizontal = 0;
    int vertical = 0;
    int mina = 0;
    
    //images para las celdas
    Image oculta = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_unexplored.png"));
    Image minaExplotada = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_mine_hit.png"));
    Image banderita = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_flag.png"));
    Image alrrededor0 = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_0.png"));
    Image alrrededor1 = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_1.png"));
    Image alrrededor2 = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_2.png"));
    Image alrrededor3 = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_3.png"));                    
    Image alrrededor4 = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_4.png"));                      
    Image alrrededor5 = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_5.png"));                      
    Image alrrededor6 = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_6.png"));
    Image alrrededor7 = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_7.png"));                       
    Image alrrededor8 = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_8.png"));
    ImageView imagen = new ImageView();
    
    
    /**
     * Crea el entorno gráfico del juego con todas las celdas sin descubrir.
     * @param horizontal Número de celdas en horizontal.
     * @param vertical Número de celdas en vertical.
     */
    BuscaMinaDibujo(int horizontal, int vertical, int mina) {
        
//        tamañoImagen = oculta.getWidth();
        this.horizontal = horizontal;
        this.vertical = vertical;        
        this.mina = mina;
        
        for(int i=0; i<horizontal; i++){
            for(int j=0; j<vertical; j++){
                ImageView imagen = new ImageView();
                imagen.setImage(oculta);
                root.add(imagen, i, j);
            }
        }
        this.buscaMinaTablero = new BuscaMinaTablero(horizontal, vertical, mina); 
        hboxInferior.getChildren().add(root);
        hboxInferior.setAlignment(Pos.CENTER);

        
//        hboxSuperior.getChildren().add(BuscaMinas.superior);
//        hboxSuperior.setAlignment(Pos.CENTER);
        
//        this.getChildren().addAll(hboxSuperior, hboxInferior);
        this.getChildren().addAll(BuscaMina.partidaTerminada, BuscaMina.menu, hboxInferior);
        this.setAlignment(Pos.CENTER);
    }
    
    
    /**
     * Actualiza el juego cada vez que se pulsa una celda del juego.
     */
    public void actualizar(int horizontal, int vertical, int mina){
        
//        this.buscaMinaTablero = new BuscaMinaTablero(horizontal, vertical, mina); 
        int contador = 0;
        
        for(int i=0; i<horizontal; i++){
            for(int j=0; j<vertical; j++){
                ImageView imagen2 = (ImageView)root.getChildren().get(contador);
                contador++;
                if(buscaMinaTablero.tablero[i][j].getEstado() != BuscaMinaCelda.SIN_DESCUBRIR){
                    if(buscaMinaTablero.tablero[i][j].getEstado() == BuscaMinaCelda.DESCUBRIERTA){
                        if(buscaMinaTablero.tablero[i][j].getMina()){
                            imagen2.setImage(minaExplotada);
                        }
                        else{
                            byte numMinasColindantes = buscaMinaTablero.tablero[i][j].getContador();
                            switch(numMinasColindantes){
                                case 0:
                                    imagen2.setImage(alrrededor0);
                                    break;
                                case 1:
                                    imagen2.setImage(alrrededor1);
                                    break;    
                                case 2:
                                    imagen2.setImage(alrrededor2);
                                    break;    
                                case 3:
                                    imagen2.setImage(alrrededor3);
                                    break;    
                                case 4:
                                    imagen2.setImage(alrrededor4);
                                    break;    
                                case 5:
                                    imagen2.setImage(alrrededor5);
                                    break;    
                                case 6:
                                    imagen2.setImage(alrrededor6);
                                    break;    
                                case 7:
                                    imagen2.setImage(alrrededor7);
                                    break;    
                                case 8:
                                    imagen2.setImage(alrrededor8);
                                    break;   
                            }
                        }
                    }
                    else{
                        if(buscaMinaTablero.tablero[i][j].getEstado() == BuscaMinaCelda.BANDERITA){
                            imagen2.setImage(banderita);
                        }
                    } 
                }
                else{
                    imagen2.setImage(oculta);
                }
            }
        }    
    }
    
    /**
     * Reinicia la partida con otro tablero distinto.
     * @param horizontal Es el número de celdas horizontales que contiene el juego.
     * @param vertical Es el número de celdas verticales que contiene el juego.
     * @param mina Es el número de minas que contiene el juego.
     */
    public void reiniciar(int horizontal, int vertical, int mina){
        
        //Borrado del juego anterior.
        root.getChildren().clear();
        //Redimensión del tablero.
        buscaMinaTablero = new BuscaMinaTablero(horizontal, vertical, mina); 
        for(int i=0; i<horizontal; i++) {
            for(int j=0; j<vertical; j++) {
                ImageView imagen3 = new ImageView();
                imagen3.setImage(oculta);
                root.add(imagen3, i, j);
            }
        }
    }
    
    

    
    
}
