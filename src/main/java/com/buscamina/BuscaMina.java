package com.buscamina;

import com.sun.glass.ui.PlatformFactory;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
//import com.gluonhq.charm.down.common.PlatformFactory;

public class BuscaMina extends Application {

    //Vreacion de objetos y variables.
    int horizontal = 5;
    int vertical = 5;
    int mina = 3;
    
    boolean ponerBandera;
    public static HBox menu = new HBox();
    public static Label partidaTerminada = new Label("Partida en proceso.");
    double tamañoEscala = 61;
            
    @Override
    public void start(Stage stage) {
        
        BuscaMinaDibujo buscaMinaDibujo = new BuscaMinaDibujo(horizontal, vertical, mina);
//        //Vreacion de objetos y variables.
//        
////        BuscaMinaTablero buscaMinaTablero = new BuscaMinaTablero(horizontal, vertical, mina);
//        //Creación de las cajas, del botón y alineación.
//        HBox contenedor = new HBox();
//        TextField posicionHorizontal = new TextField("");
//        TextField posicionVertical = new TextField("");
//        Button btn = new Button("Comprobar");
//        Button  bandera = new Button("Bandera");
//        Button probar = new Button("Probar");
//        contenedor.getChildren().addAll(posicionVertical, posicionHorizontal, btn, bandera, probar);
//        contenedor.setAlignment(Pos.CENTER);
//        
//        StackPane root = new StackPane(contenedor);
//        
//        //Acciones de botones.
//        //Acción del botón Comprobar.
//        btn.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
//            @Override
//            public void handle(javafx.event.ActionEvent event) {
//                int coordenadaX = Integer.parseInt(posicionHorizontal.getText());
//                int coordenadaY = Integer.parseInt(posicionVertical.getText());
//                buscaMinaTablero.descubrirCelda(coordenadaX, coordenadaY);
////                registro.log(Level.WARNING, buscaMinaTablero.toString());
//            }
//
//        });
//        
//        
//        //Acción del botón Bandera.
//        bandera.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
//            @Override
//            public void handle(javafx.event.ActionEvent event) {
//                int coordenadaX = Integer.parseInt(posicionHorizontal.getText());
//                int coordenadaY = Integer.parseInt(posicionVertical.getText());
//                buscaMinaTablero.colocarBanderita(coordenadaX, coordenadaY);
//            }
//
//        });
//        
//        Scene scene1 = new Scene(buscaMinaDibujo, 600, 300);
//        //Accion del botón probar.
//        probar.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
//            @Override
//            public void handle(javafx.event.ActionEvent event) {
//                
//                stage.setScene(scene1);
//            }
//
//        });

        
        //A partir de aqui pertenece a la escena con entorno grafico.
//        Scene scene1 = new Scene(juegoGrafico, 600, 300);
        
        
        //Coordenadas.
        buscaMinaDibujo.root.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle (MouseEvent event) {
                
                double coordenadaX = event.getX();
                double coordenadaY = event.getY();
                
                int celdaPosicionX = (int)coordenadaX/61;
                int celdaPosicionY = (int)coordenadaY/61;
                
                System.out.println(celdaPosicionX);
                System.out.println(celdaPosicionY);
                if(ponerBandera){
                    BuscaMinaDibujo.buscaMinaTablero.colocarBanderita(celdaPosicionX, celdaPosicionY);
                  //registro.log(Level.FINEST, BuscaMinasTablero.toString());
                }
                else{
                    if(BuscaMinaDibujo.buscaMinaTablero.descubrirCelda(celdaPosicionX, celdaPosicionY)){
                        BuscaMinaDibujo.buscaMinaTablero.abandonar();
                        partidaTerminada.setText("Has perdido.");
                    }
                    else{
                        if(BuscaMinaDibujo.buscaMinaTablero.finPartida()){
                            partidaTerminada.setText("Has ganado.");
                        }
                    }
                }
                ponerBandera = false;
                buscaMinaDibujo.actualizar(horizontal, vertical, mina);   
            }
        });
        
        
        //Menu para el juego.
        Button reiniciar = new Button("Reiniciar");
        Button bandera = new Button("Bandera");
        Button dificultad = new Button("Dificultad");
        ComboBox dificultadElegida = new ComboBox();
        dificultadElegida.getItems().addAll("Principiante", "Intermedio", "Experto");
        dificultadElegida.setValue("Principiante");
        menu.setSpacing(2);
        menu.getChildren().addAll(reiniciar, bandera, dificultad, dificultadElegida);
        menu.setAlignment(Pos.CENTER);
        partidaTerminada.setAlignment(Pos.CENTER);
        
        // Reiniciar partida.
        reiniciar.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                
                buscaMinaDibujo.reiniciar(horizontal, vertical, mina);
                buscaMinaDibujo.actualizar(horizontal, vertical, mina);
                partidaTerminada.setText("Partida en proceso.");
                
            }
            
        });
        
        
        //Colocar una bandera.
        bandera.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                ponerBandera = true;
            }
            
        });
        
        
        //Escoger dificultad.
        dificultad.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                
                if(String.valueOf(dificultadElegida.getValue()) == "Principiante"){
                    horizontal = 5;
                    vertical = 5;
                    mina = 3;    
                    tamañoEscala = 61;
                }
                else{
                    if(String.valueOf(dificultadElegida.getValue()) == "Intermedio"){
                        horizontal = 8;
                        vertical = 8;
                        mina = 10;    
                        tamañoEscala = 97.6;
                    }
                    else{
                        horizontal = 10;
                        vertical = 10;
                        mina = 15;   
                        tamañoEscala = 122;
                    }
                }
                buscaMinaDibujo.reiniciar(horizontal, vertical, mina);
                buscaMinaDibujo.actualizar(horizontal, vertical, mina);
            }

        });
        
        
        //Para escalar la escena.
        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
        Scene scene1 = new Scene(buscaMinaDibujo, visualBounds.getWidth(), visualBounds.getHeight());
        buscaMinaDibujo.setScaleX(visualBounds.getWidth() / (horizontal * tamañoEscala));
        buscaMinaDibujo.setScaleY(visualBounds.getHeight() / (vertical * tamañoEscala));
//        menu.setScaleX(visualBounds.getWidth() / (horizontal * tamañoEscala));

        
//        if(PlatformFactory.getPlatform().getName() == "Android"){
        
            /**
            * Cerrar la aplicacion con el boton escape.
            */
            scene1.setOnKeyPressed(new EventHandler<KeyEvent>() {
                public void handle(KeyEvent ex) {
                    if(ex.getCode() == KeyCode.ESCAPE){
                        stage.close();
                    }
                }

            });
//        }
        

        //Imagen de la ventana.
        stage.getIcons().add(new Image(BuscaMina.class.getResourceAsStream("/icon.png")));

        //Mostrar escena.
        stage.setScene(scene1);
        stage.show();
    }

}
