/*

package com.buscamina;
 
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
 
 
 

public class graficos extends HBox{
    int mode = 0;
    boolean end;
    byte controljuego;
    public final byte ENJUEGO = 1;
    final byte GANADO = 2;
    final byte PERDIDO = 0;
    Tablero tablero;
    Image imagenNoVisible = new Image(getClass().getResourceAsStream("/Minesweeper_unexplored.png"));
    Image imagenVisible = new Image(getClass().getResourceAsStream("/Minesweeper_explored.png"));
    Image imagenBandera = new Image(getClass().getResourceAsStream("/Minesweeper_flag.png"));
    Image imagenBomba = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_mine_hit.png"));
    Image imagen_uno = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_1.png"));
    Image imagen_dos = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_2.png"));
    Image imagen_tres = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_3.png"));
    Image imagen_cuatro = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_4.png"));
    Image imagen_cinco = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_5.png"));
    Image imagen_seis = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_6.png"));
    Image imagen_siete = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_7.png"));
    Image imagen_ocho = new Image(getClass().getResourceAsStream("/Minesweeper_LAZARUS_61x61_8.png"));
    int tamaño1;
    int tamaño2;
    int numMinas;
    GridPane panel = new GridPane();
    VBox vbox = new VBox();
    HBox botones = new HBox();
    Button banderagrafico = new Button("Poner Bandera");
    Button suicide = new Button("Sucide");
    Button reset = new Button("Reset");
    Label titulo = new Label("Buscaminas:");
    final static Logger LOGGER = Logger.getLogger("myLogger");
    
    Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
//    Graficos(int tamaño1, int tamaño2, Tablero tablero){
    graficos(int tamaño1, int tamaño2, int numMinas){
         
    graficos(int dificultad){
        switch(dificultad){
            case 0:
                this.tamaño1 = 8;
                this.tamaño2 = 8;
                this.numMinas = 10;
                break;
            case 1:
                this.tamaño1 = 16;
                this.tamaño2 = 16;
                this.numMinas = 40;
               break;
           case 2:
               this.tamaño1 = 16;
               this.tamaño2 = 30;
               this.numMinas = 99;
               break;
       }
       this.tablero = new Tablero(tamaño1, tamaño2, numMinas);
       this.tamaño1 = tamaño1;
       this.tamaño2 = tamaño2;
       
        for(int x=0; x<tamaño1; x++) {
            for(int y=0; y<tamaño2; y++) {
                ImageView imageViewElement = new ImageView();
                imageViewElement.setImage(imagenNoVisible);
                panel.add(imageViewElement, y, x);
            }
        }
        botones.getChildren().addAll(banderagrafico,suicide,reset);
        botones.setStyle("-fx-spacing: 50px;");
        titulo.setStyle("-fx-size: 50px;");
        vbox.setStyle("-fx-spacing: 5px;");
        
//        panel.setScaleX(visualBounds.getWidth()/(tamaño1*61));
//        panel.setScaleY(visualBounds.getHeight()/(tamaño2*61));
        vbox.getChildren().addAll(titulo,botones,panel);
        this.setAlignment(Pos.CENTER);
        this.getChildren().add(vbox);
    
        reset.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                reiniciar(tamaño1,tamaño2);
            }
        });
        
        suicide.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                if(controlarEstado(end) == ENJUEGO){
                    suicide();    
                }
            }
        });
        
        panel.setOnMouseClicked(new EventHandler<MouseEvent>(){
            public void handle (MouseEvent mouseEvent) {        
                int fila = (int)mouseEvent.getY()/61;
                int columna = (int)mouseEvent.getX()/61;
                click(fila, columna);
                actualizar();
            }
        });
        
        banderagrafico.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent e){
                modobandera();
            }
        });
    }

    public Tablero getTablero() {
        return tablero;
    }
    // Metodo que cambia la variable para poder poner bandera
    public void modobandera(){
        mode = 1;
        System.out.println("mode"+ mode);
    }
    // Metodo que se ejecuta al hacer click dode puede destapar celdas y poner banderas
    public void click(int fila, int columna ){
        if(controlarEstado(end) == ENJUEGO){
            System.out.println("Se ha Hecho clik en " + fila + " y " + columna);
            if(tablero.matriz[fila][columna].getEstado() == Celdas.NOVISIBLE || tablero.matriz[fila][columna].getEstado()
                        == Celdas.MARCADA){
                if(mode==0){
                    end = tablero.destaparCeldas(fila,columna);
                }else{
                    tablero.ponerBandera(fila,columna);
                    mode = 0;
                    System.out.println("mode:"+mode);
                };
                    LOGGER.log(Level.FINE,toString());
                }else{
                    LOGGER.log(Level.FINE,"\n"+"Esta celda esta destapada");
                }
                controlarEstado(end);
        }
    }
    
    
    public void actualizar(){
        int contador=0;
        for(int x=0; x<tamaño1; x++) {
            for(int y=0; y<tamaño2; y++) {
                ImageView imageViewElement =(ImageView)panel.getChildren().get(contador);
                if(tablero.matriz[x][y].getEstado() == Celdas.NOVISIBLE){
                    imageViewElement.setImage(imagenNoVisible);
               }else{
                    if(tablero.matriz[x][y].getEstado() == Celdas.MARCADA){
                        imageViewElement.setImage(imagenBandera);
                    }else{        
                        switch(tablero.matriz[x][y].getContador()){
                            case 1: 
                                imageViewElement.setImage(imagen_uno);
                                break;
                            case 2: 
                                imageViewElement.setImage(imagen_dos);
                                break;
                            case 3:
                                imageViewElement.setImage(imagen_tres);
                                break;
                            case 4:
                                imageViewElement.setImage(imagen_cuatro);
                                break;
                            case 5: 
                                imageViewElement.setImage(imagen_cinco);
                                break;
                            case 6:
                                imageViewElement.setImage(imagen_seis);
                                break;
                            case 7:
                                imageViewElement.setImage(imagen_siete);
                                break;
                            case 8:
                                imageViewElement.setImage(imagen_ocho);
                                break;
                            case 9:
                                imageViewElement.setImage(imagenBomba);
                                break;
                            default:
                               imageViewElement.setImage(imagenVisible);
                               break;
                        }
                    }
                }
                contador++;
            }
        }
    }
    // El jugador termina el juego 
    public void suicide(){
        for(int x=0; x<tamaño1; x++) {
            for(int y=0; y<tamaño2; y++) {
               tablero.matriz[x][y].setEstado(Celdas.VISIBLE);
               this.actualizar();
            }
        }
    }
    // Crea un trablero nuevo, elimina los hijos del gridpane y crea nuevos
    public void reiniciar(int tamaño1, int tamaño2){
        tablero = new Tablero(8,8,5);
        panel.getChildren().clear();
        for(int x=0; x<tamaño1; x++) {
            for(int y=0; y<tamaño2; y++) {
                ImageView imageViewElement = new ImageView();
                imageViewElement.setImage(imagenNoVisible);
                panel.add(imageViewElement, y, x);
            }
        }
        end = false;
        controlarEstado(end);
        titulo.setText("Buscaminas: ");
    }
    // Controlar si el jugador ha ganado, ha perdido o esta jugando llamando a FInjuego
    public byte controlarEstado(boolean end){
        this.end = end;
        if(tablero.finjuego()){
            controljuego = GANADO;
            titulo.setText("HAS GANADO");
        }else{
            controljuego = ENJUEGO;
        }
        if(end == true){
            controljuego = PERDIDO;
            this.suicide();
            titulo.setText("HAS PERDIDO");
        }  
        return controljuego;
    }
}







*/